class Encarcelado {
    _palabra;
    _letra;
    // constructor se ejecuta cuando creo el objeto de la clase 
    constructor(valor) {
        this._palabra = valor
    }

    set palabra(valor) {
        this._palabra = valor;
    }

    iniciarJuego() {
        let a = this._palabra.length
        let boton;
        let formulario = document.getElementById("formulario");
        for (let i = 0; i < a; i++) {
            boton = document.createElement("input");
            boton.setAttribute("type", "button");
            formulario.appendChild(boton);
        }
    }

    verificar() {
        let letra = document.getElementById("letra").value;
        let res = false;

        for (let i = 0; i <= this._palabra.length; i++) {
            if (letra === this._palabra.charAt(i)) {
                res = true;
                alert("yes bb");
            }
        }
        return res;
    }
}
// palabra que se usa como default 
let miJuego = new Encarcelado("universidad");

miJuego.iniciarJuego();
miJuego.verificar();